import time

starttime = time.time()
c = 0
while c < 100000:
    arr = [3,4,1,3,5,1,92,2,4124,424,52,12]
    i = 0
    while i < len(arr):
        y = 0
        while y < len(arr) - 1:
            if arr[y+1] < arr[y]:
                t = arr[y]
                arr[y] = arr[y + 1]
                arr[y + 1] = t
            y += 1
        i += 1
    c += 1
endtime = time.time()
print(endtime - starttime)
print(endtime)
print(starttime)
