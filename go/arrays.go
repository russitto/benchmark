package main

import (
	"time"
)

func main() {
	start := time.Now()

	for c := 0; c < 100000; c++ {
		array := [12]int{3, 4, 1, 3, 5, 1, 92, 2, 4124, 424, 52, 12}
		for i := 0; i < len(array); i++ {
			for y := 0; y < len(array) - 1; y++ {
				if array[y+1] < array[y] {
					t := array[y+1]
					array[y+1] = array[y]
					array[y] = t
				}
			}
		}
	}
	end := time.Now()
	elapsed := end.Sub(start)
	println("nanosecs", elapsed)
}
