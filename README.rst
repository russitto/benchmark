=========
Benchmark
=========

Simple bubble sort benchmark.

---------
Languages
---------

* https://crystal-lang.org/
* https://golang.org/
* https://nodejs.org/
* https://php.net/
* https://www.python.org/
